<?php
session_start();
error_reporting(E_ALL);

/*
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!!   This is part of a ctf event!                !!!
 * !!!   in this code are several insecure things!   !!!
 * !!!   Dont use it as reference!                   !!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

if(isset($_POST["logout"])) { session_unset(); }

$uploaddir = 'useruploads/' . session_id() . "/";
$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

?>

<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Kukulkan</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>

<header id="header" id="home">
    <div class="container">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="index.html"><img src="img/logo.png" alt="" title=""/></a>
            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="#home">Home</a></li>
                    <li><a href="#offer">Unsere Leistungen</a></li>
                    <li><a href="#aboutus">Über uns</a></li>

                    <li><a href="#patron">Namenspatron</a></li>

                    <li><a href="#upload">Upload</a></li>
                    <li><a href="#dateien">Dateien</a></li>

                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </div>
</header><!-- #header -->


<!-- start banner Area -->
<section class="banner-area" id="home">
    <div class="container">
        <div class="row fullscreen d-flex align-items-center justify-content-center">
            <div class="banner-content col-lg-7">
                <h1>
                    Wir denken komplexe Abläufe neu!
                </h1>
                <p class="pt-20 pb-20">
                    Anfang des Jahres 2020 gründeten wir die Firma Kukulkan. Unseren Kunden bieten wir eine persönliche
                    Beratung und Betreuung, schnelle Lieferung mit Zufriedenheits- und Qualitätsgarantie!</p>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->

<!-- Start we-offer Area -->
<section class="we-offer-area section-gap" id="offer">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-10">
                <div class="title text-center">
                    <h1 class="mb-10">Unsere Leistungen für Sie</h1>
                    <p>Sprechen Sie uns an!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="single-offer d-flex flex-row pb-30">
                    <div class="icon">
                        <img src="img/s1.png" alt="">
                    </div>
                    <div class="desc">
                        <a href="#"><h4>Reichhaltige Auswahl</h4></a>
                        <p>
                            Individuelle Speziallösungen sind unser Standard!<br>
                            Durch komplett individuelle Lösungen und Fertigung mit modernsten Standards bleibt Ihnen
                            jederzeit die Möglichkeit an allen Stellschrauben zu drehen.
                        <p></p>
                        Denn unser Produkt ist Ihr Produkt!
                        </p>
                    </div>
                </div>
                <div class="single-offer d-flex flex-row pb-30">
                    <div class="icon">
                        <img src="img/s3.png" alt="">
                    </div>
                    <div class="desc">
                        <a href="#"><h4>Wir helfen Ihnen auch bei schwierigen Anfragen</h4></a>
                        <p>
                            Ihr Anliegen ist sehr speziell? Wir haben Erfahrung damit!<br>
                            Zögern Sie nicht uns auch mit ungewöhnlichen Spezifikationen zu kontaktieren, wir bekommen
                            das schon hin!
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="single-offer d-flex flex-row pb-30">
                    <div class="icon">
                        <img src="img/s2.png" alt="">
                    </div>
                    <div class="desc">
                        <a href="#"><h4>Einzigartige Lösungen für einzigartige Kunden</h4></a>
                        <p>
                            Wir verstehen Ihre Bedürfnisse und machen Sie zu unseren.<br>
                            Einzigartige Anforderungen erfordern einzigartige Lösungen. Vertrauen Sie uns und wir machen
                            Ihnen ihr individuelles Angebot.
                        </p>
                    </div>
                </div>
                <div class="single-offer d-flex flex-row pb-30">
                    <div class="icon">
                        <img src="img/s4.png" alt="">
                    </div>
                    <div class="desc">
                        <a href="#"><h4>Alles in einem Pakte</h4></a>
                        <p>
                            Von der gemeinsamen Spezifizierung Ihrer Wünsche, über das Design, Materialprüfungen und
                            Fertigung, bis zur Lieferung in Ihr Werk, unser Angebot ist das Komplettpaket.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End we-offer Area -->

<!-- Start about us Area -->
<section class="testomial-area section-gap" id="aboutus">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Unser Team</h1>
                    <p>Unser Team besteht aus technikbegeisterten und entdeckungsfreudigen Mitarbeitern. Die
                        Begeisterung für Flugzeuge verbindet uns und treibt uns an.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="active-tstimonial-carusel">

                <div class="single-testimonial item">
                    <img class="mx-auto" src="img/personen/ceo_frank_berlepsch.jpeg" alt=""
                         width="150" height="150">
                    <p class="desc">
                        Als Gründungsmitglied von Kukulkan ist Frank Berlepsch einer der erfahrensten Mitarbeiter. Viele
                        Jahre arbeitete er in verschiedenen Abteilungen einer Fluggesellschaft, wo er Gabriella
                        Sanchez-Ortiz kennenlernte. Gemeinsam machten sie sich selbständig und gründeten Kukulkan.
                    </p>
                    <h4>Frank Berlepsch</h4>
                    <p>
                        CEO
                    </p>
                </div>

                <div class="single-testimonial item">
                    <img class="mx-auto" src="img/personen/cto_gabriela_sanchez-ortiz.jpeg" alt=""
                         width="150" height="150">
                    <p class="desc">
                        Im mexikanischen Valladolid aufgewachsen gelangte die Informatikerin durch ihre berufliche
                        Tätigkeit in einem großen Luftfahrtunternehmen nach Deutschland. Unter ihrer Ägide wird die
                        Entwicklung von unserem Portfolio vorangetrieben. Aus der Mythologie der Maya, die Teil des
                        mexikanischen Kulturerbes ist, ging unser Firmenname hervor.
                    </p>
                    <h4>Gabriella Sanchez-Ortiz</h4>
                    <p>
                        CTO
                    </p>
                </div>

                <div class="single-testimonial item">
                    <img class="mx-auto" src="img/personen/hr_svea_jensen.jpeg" alt=""
                         width="150" height="150">
                    <p class="desc">
                        Unser fröhliches Nordlicht Svea kümmert sich um alle Personalangelegenheiten und hat immer ein
                        offene Ohr.
                    </p>
                    <h4>Svea Jensen</h4>
                    <p>
                        HR
                    </p>
                </div>

                <div class="single-testimonial item">
                    <img class="mx-auto" src="img/personen/admin_lucas_alves.jpeg" alt=""
                         width="150" height="150">
                    <p class="desc">
                        Lucas kümmert sich liebevoll um die Einrichtung und Pflege unserer IT. Ob Webserver,
                        Kollaborationsmöglichkeiten oder die Einrichtung unserer Desktops, Lucas ist ein wahres
                        Multitalent!
                    </p>
                    <h4>Lucas Alves</h4>
                    <p>
                        IT
                    </p>
                </div>

                <div class="single-testimonial item">
                    <img class="mx-auto" src="img/personen/ingenieur_flugzeugbau_sebastian_fruehschuetz.jpeg" alt=""
                         width="150" height="150">
                    <p class="desc">
                        Sebastian ist die treibende Kraft in der Entwicklung unserer Produkte. Er bedient unsere
                        CAD-Software wie kein Zweiter.
                    </p>
                    <h4>Sebastian Frühschütz</h4>
                    <p>
                        Ingenieur Flugzeugbau
                    </p>
                </div>

                <div class="single-testimonial item">
                    <img class="mx-auto" src="img/personen/ingenieur_alexander_arnold.jpeg" alt=""
                         width="150" height="150">
                    <p class="desc">
                        Gleich nach seinem Studium der Maschineningenieurwissenschaften an der ETH-Zürich stieg Alex bei
                        Kukulkan ein. Gemeinsam mit Sebastian leitet er die Forschung und Entwicklung unseres
                        Fachbereichs der Faserverbundwerkstoffe.
                    </p>
                    <h4>Alexander Arnold</h4>
                    <p>
                        Ingenieur
                    </p>
                </div>

                <div class="single-testimonial item">
                    <img class="mx-auto" src="img/personen/officecat_pepito.jpg" alt=""
                         width="150" height="150">
                    <p class="desc">
                        Einen Office-Dog hat jeder, aber nur wir haben eine mexikanische Office-Cat! Pepito schnurrt
                        sich seit Gründung der Firma in unsere Herzen und in die unserer Kunden.
                    </p>
                    <h4>Pepito</h4>
                    <p>
                        Office-Cat
                    </p>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- End about us Area -->

<!-- Start Align Area -->
<section class="we-offer-area section-gap" id="patron">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <h3 class="mb-30">Namenspatron</h3>
            <div class="row">
                <div class="col-md-3">
                    <img src="img/YaxchilanDivineSerpent.jpg" alt="" class="img-fluid">
                    <a href="https://commons.wikimedia.org/w/index.php?curid=147279">Abbildung Gemeinfrei</a>
                </div>
                <div class="col-md-9 mt-sm-20">
                    <p>
                        Unser Namensgeber ist die mittelamerikanische Schlangengottheit Quetzalcoatl, die mit den
                        bunten, bis zu einem Meter langen, Schwanzfedern der lateinamerikanischen Vogelart Quetzal
                        geschmückt ist.<br>
                        <br>
                        Der Quetzalcoatl, In der Sprache der Maya "K'uk'ulkan", ist der Gott des Windes, des
                        Himmels,
                        der Erde, ein Schöpfergott und somit der ideale Pate für unser Unternehmen!<br>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Align Area -->

<!-- Start Sample Area -->
<section class="testomial-area section-gap" id="upload">
    <div class="container">
        <h3 class="text-heading">Upload</h3>
        <p class="sample-text">
            Auf mehrfachen Wunsch bieten wir Ihnen hier die Möglichkeit, uns Fertigungszeichnungen zukommen zu
            lassen.<br>

            Laden Sie diese einfach hier hoch.
        </p>
            
        <!-- Danke an das php-Manual für diese einfache Anleitung!-->
        <!-- Sogar inklusive Schutz gegen Dateiupload-Attacken!-->
        <!-- https://www.php.net/manual/de/features.file-upload.post-method.php -->
        <!-- Die Encoding-Art enctype MUSS wie dargestellt angegeben werden -->
        <form enctype="multipart/form-data" action="#upload" method="POST">
            <!-- MAX_FILE_SIZE muss vor dem Dateiupload Input Feld stehen -->
            <input type="hidden" name="MAX_FILE_SIZE" value="3000000"/>
            <!-- Der Name des Input Felds bestimmt den Namen im $_FILES Array -->
            <input name="userfile" type="file" placeholder="Dateiname" onfocus="this.placeholder = ''"
                onblur="this.placeholder = 'Dateiname'" required class="single-input"/><br>
            <!--<a href="#" >Primary<span class="lnr lnr-arrow-right"></span></a>-->
            <input type="submit" value="Send File" class="genric-btn primary-border circle arrow"/>
        </form>
        <?php
            // In PHP kleiner als 4.1.0 sollten Sie $HTTP_POST_FILES anstatt
            // $_FILES verwenden.

            if (isset($_FILES['userfile'])) {
                mkdir($uploaddir);
                if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
                    echo "Erfolgreich hochgeladen\n";
                } else {
                    echo "Möglicherweise eine Dateiupload-Attacke!\n";
                }
            }
        ?>
            
    </div>
</section>
<!-- End Sample Area -->

<section class="we-offer-area section-gap" id="dateien">
    <div class="container">
        <div class="section-top-border">
            <h3 class="mb-30">Meine hochgeladenen Dateien</h3>
            <div class="progress-table-wrap">
                <div class="progress-table">
                    <div class="table-head">
                        <div class="serial">#</div>
                        <div class="serial">Datei</div>
                    </div>
                    <?php
                    $directoryFiles = array_diff(scandir($uploaddir), array('..', '.'));

                    $format = '
                                    <div class="table-row">
                                    <div class="serial">%s</div>
                                    <div class="serial"><a href="%s%s">%s</a></div>                            
                                    </div>
                                    ';

                    foreach ($directoryFiles as $k => $v) {
                        printf($format, ($k - 1), $uploaddir, $v, $v);
                    }

                    ?>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- start footer Area -->
<footer class="footer-area section-gap" id="footer">
    <div class="container">
        <div class="row footer-bottom d-flex justify-content-between">
            <p class="col-lg-8 col-sm-12 footer-text m-0 text-white">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a
                        href="https://colorlib.com" target="_blank">Colorlib</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
            <div class="col-lg-4 col-sm-12 footer-social">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a>
            </div>
            
            <div class="col-lg-4 col-sm-12 footer-text">
            
                <?php            
                /*
                * Login-Formular ausgefüllt
                */
                if(isset($_POST["username"]) && isset($_POST["username"])) {
                
                    $inputUsername = trim($_POST['username']);
                    $inputHash = md5(trim($_POST['password']));
                    
                    $database = file("database/database.db");
                    
                    foreach($database as $entry) {
                        $dbUser= trim(explode("<",$entry)[0]);
                        $dbHash = trim(explode("<",$entry)[1]);
                        
                        if ($inputUsername == $dbUser && $inputHash == $dbHash) {
                            session_unset(); // beware, this is not enough! But it is not part of this ctf
                            $_SESSION["loggedin"] = true;
                            $_SESSION["username"] = $inputUsername;
                            break;
                        }
                    }
                }
                
                if(isset($_SESSION["loggedin"])) {
                    $username = $_SESSION["username"];
                    echo "Eingeloggt als $username<br>";
                    echo "Es gibt keine Adminmodus (Wirklich nicht!) Die Flag ist das Passwort des Benutzers 'admin'<br>";
                    echo "<a href='https://xkcd.com/792/'>Aber hier ZUSÄTZLICH ein Comic zur Belohnung!</a>";
                    ?>
                    <form action = "#footer" method = "post">
                        <button class = "genric-btn primary circle arrow" type = "submit" name = "logout">Logout</button>
                    </form>
                    <?php
                    
                } else {
                    ?>                    
                    
                        <p onclick="$('#adminLogin').toggleClass('d-none');">Adminlogin</p>
                        <div id="adminLogin" class="d-none">
                            <form action = "#footer" method = "post">
                                <input type = "text" class = "single-input" name = "username" placeholder = "username" required autofocus>
                                <input type = "password" class = "single-input" name = "password" placeholder = "password" required>
                                <button class = "genric-btn primary circle arrow" type = "submit" name = "login">Login</button>
                            </form>
                        </div>
                    
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</footer>
<!-- End footer Area -->

<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="js/easing.min.js"></script>
<script src="js/hoverIntent.js"></script>
<script src="js/superfish.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
</body>
</html>
